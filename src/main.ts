import { createApp } from "vue";
import "./style.css";
import App from "./App.vue";

//theme
import "primevue/resources/themes/lara-light-indigo/theme.css";

import "primevue/resources/primevue.min.css";
import "primeicons/primeicons.css";
import { router } from "./router";

import PrimeVue from "primevue/config";
import Menubar from "primevue/menubar";
import InlineMessage from "primevue/inlinemessage";
import Button from "primevue/button";

const app = createApp(App);
app.use(PrimeVue, { ripple: true });
app.component("Button", Button);
app.component("Menubar", Menubar);
app.component("InlineMessage", InlineMessage);
app.use(router);

app.mount("#app");
