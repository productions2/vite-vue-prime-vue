# Vue 3 + TypeScript + Vite + vue-Prime

## Estructura del proyecto

```
│   .gitignore
│   index.html
│   package.json
│   README.md
│   tsconfig.json
│   tsconfig.node.json
│   vite.config.ts
│   yarn.lock
│
├───.vscode
│       extensions.json
│
├───node_modules
|
├───public
│       vite.svg
│
└───src
    │   App.vue
    │   main.ts
    │   router.ts
    │   style.css
    │   vite-env.d.ts
    │
    ├───assets
    │       vue.svg
    │
    └───components
            HelloWorld.vue
            InlineMessage.vue
            Nabvar.vue
```

## instalar prime-vue

1. Intalar prime-vue

```sh
yarn add primevue
```

## como usar un componente

- imicialmente el archivo main.ts se ve de esta manera

```js
import { createApp } from "vue";
import "./style.css";
import App from "./App.vue";

createApp(App).mount("#app");
```

- Debe modificar la estructura:

```js
import { createApp } from "vue";
import "./style.css";
import App from "./App.vue";

const app = createApp(App);

app.mount("#app");
```

- Ahora veremos como importar componentes

  - Para usar un componente de `prime-vue` debe ir al archivo `main.ts`

```ts
import { createApp } from "vue";
import "./style.css";
import App from "./App.vue";

// importamos prime-vue
import PrimeVue from "primevue/config";

// utilidades para temas
import "primevue/resources/themes/lara-light-indigo/theme.css";
// estilos
import "primevue/resources/primevue.min.css";
// iconos
import "primeicons/primeicons.css";

import { router } from "./router";

// importamos el componente Button
import Button from "primevue/button";

const app = createApp(App);

// hara que vue use a PrimeVue
app.use(PrimeVue, { ripple: true });

// cada componnete importado debe implementarese de esta manera para que vue pueda usarlo
app.component("Button", Button);

app.use(router);

app.mount("#app");
```

- puede usar
  - [PrimeFlex](https://primeflex.org/) un Compañero perfecto de utilidad CSS
